let expNet = [
  'Network maintenance according to customer needs (Product Cisco).',
  'Analyze the cause and solve network problems.',
  'Implement the Network system for customers and design the network structure.',
  'Support operation support / Trouble shooting to the assigned Site.',
  'Support Pre Sales work with Sales team to receive Requirement with new customers about Project Network.',
  'Maintain Service Level Agreement (SLA) in accordance with the agreement.',
];
let expPro = [
  'Website and software application designing, building, or maintaining.',
  'Using scripting or authoring languages, management tools, content creation tools, applications, and digital media.',
  'Identifying problems uncovered by customer feedback and testing, and correcting or referring problems to appropriate personnel for correction.',
  'Determining user needs by analyzing technical requirements.',
];
let expHandyWing = [
  'Website and software application designing, building, or maintaining.',
  'Using scripting or authoring languages, management tools, content creation tools, applications, and digital media.',
  'Identifying problems uncovered by customer feedback and testing, and correcting or referring problems to appropriate personnel for correction.',
  'Determining user needs by analyzing technical requirements.',
];

let aow = ['LAN/WAN', 'TCP/IP', 'Router, Switch', 'Network Information'];
let skillAndTools = [
  'HTML',
  'CSS/Bootstrap,Tailwind',
  'JavaScript/ReactJS',
  'TypeScript/Angular10+',
  'JAVA/JavaSpringboot',
  'SQL/WorkBench',
  'C',
  'Netlify',
  'Git/Github',
  'jenkins',
  'Docker',
  'bitbucket',
  'VSCode',
  'Postman',
  'IntelliJ IDEA',
  'Jasper Report',
];

let expNethtml = `<ul>`;
for (const x of expNet) {
  expNethtml += `<li>${x}</li>`;
}
expNethtml += `</ul>`;
document.getElementById('expNet').innerHTML = expNethtml;

let expProhtml = `<ul>`;
for (const x of expPro) {
  expProhtml += `<li>${x}</li>`;
}
expProhtml += `</ul>`;
document.getElementById('expPro').innerHTML = expProhtml;

let expHandyWinghtml = `<ul>`;
for (const x of expHandyWing) {
  expHandyWinghtml += `<li>${x}</li>`;
}
expHandyWinghtml += `</ul>`;
document.getElementById('expHandyWing').innerHTML = expHandyWinghtml;

// let aowhtml = `<ul>`;
// for (const x of aow) {
//     aowhtml += `<li>${x}</li>`;
//   }
// aowhtml += `</ul>`;
// document.getElementById("aow").innerHTML = aowhtml;

let skillhtml = `<ul style="display: grid;grid-template-columns: auto auto;grid-gap: 0 1rem;">`;
for (const x of skillAndTools) {
  skillhtml += `<li>${x}</li>`;
}
skillhtml += `</ul>`;
document.getElementById('skill').innerHTML = skillhtml;
